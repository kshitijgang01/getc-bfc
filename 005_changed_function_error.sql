-- Write your migrate up statements here
create table users (
                       phone_number VARCHAR(12) primary key,
                       first_name VARCHAR(30) NOT NULL,
                       last_name VARCHAR(30) NOT NULL,
                       password text NOT NULL

);
create table cars(
                     car_id VARCHAR(30) PRIMARY KEY,
                     last_serviced timestamptz,
                     date_of_manufacture timestamptz,
                     last_used_date timestamptz,
                     model varchar(40) NOT NULL,
                     available bool NOT NULL
);
create table bookings(
                         car_id varchar(30) NOT NULL,
                         phone_number VARCHAR(12) NOT NULL,
                         date_booked timestamptz NOT NULL,
                         primary key(car_id,phone_number),
                         constraint fk_car foreign key(car_id) references cars(car_id) ON DELETE CASCADE,
                         constraint fk_phone_number foreign key(phone_number) references users(phone_number) ON DELETE CASCADE
);
insert into cars values('id1','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-003',TRUE);
insert into cars values('id2','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-003',TRUE);
insert into cars values('id3','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-002',TRUE);
insert into cars values('id4','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-002',TRUE);
insert into cars values('id5','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-001',TRUE);
insert into cars values('id6','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','2009-11-10T23:00:00Z','FC-001',TRUE);

CREATE or replace procedure book_car(c_id character varying,phone_number character varying,booked_when timestamptz) language plpgsql as $$
DECLARE
ph_num character varying;
BEGIN
   IF exists(Select * FROM bookings where bookings.car_id=c_id) THEN RAISE EXCEPTION 'Car is already Booked';
END IF;

update cars set available=FALSE,last_used_date=booked_when where car_id=c_id;
INSERT INTO bookings values (c_id,phone_number,booked_when);

return;
END;
$$;
CREATE or replace procedure return_car(c_id character varying,phone_number character varying,booked_when timestamptz) language plpgsql as $$
DECLARE
ph_num character varying;
BEGIN
   IF not exists(Select * FROM bookings where bookings.car_id=c_id) THEN RAISE EXCEPTION 'booking does not exist';
END IF;

update cars set available=TRUE,last_used_date=booked_when where car_id=c_id;
delete from bookings where car_id=c_id;

return;
END;
$$



---- create above / drop below ----
drop table bookings;
drop table cars;
drop table users;

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.
