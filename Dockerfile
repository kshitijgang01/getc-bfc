
FROM golang:1.19-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .



EXPOSE 8080
EXPOSE 8081

CMD ["go","run","mservice/main.go"]

