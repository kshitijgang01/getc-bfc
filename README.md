## Flying Car Booking Service

It consists of two microservices :
1. Auth Microservice
2. Booking Microservice

The booking service talks with the auth microservice to register,login and authenticate users.

![schema](./schema.png )

To run:
1. docker compose up
2. tern migrate