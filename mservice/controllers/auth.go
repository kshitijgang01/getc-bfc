package controllers

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
	"net/http"
	"time"
)

type UserInfo struct {
	FirstName   string    `json:"firstName"`
	PhoneNumber string    `json:"phoneNumber"`
	ExpiredAt   time.Time `json:"expiredAt"`
}
type Cntrl struct {
	DbPool       *pgxpool.Pool
	SessionStore map[string]UserInfo
}

type RegisterUserData struct {
	PhoneNum  string `json:"phoneNum"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Password  string `json:"password"`
}
type LoginUserData struct {
	PhoneNum string `json:"phoneNum"`
	Password string `json:"password"`
}

type Logout struct {
	SToken string `json:"sToken"`
}

func (ct Cntrl) RegisterUser(c *gin.Context) {
	var rud RegisterUserData
	err := c.BindJSON(&rud)
	if err != nil {
		c.Error(err)
		c.Status(http.StatusBadRequest)
		return
	}
	hashedPassword, err := HashPassword(rud.Password)
	if err != nil {
		c.Error(err)
		return
	}
	_, err = ct.DbPool.Exec(context.Background(), "INSERT INTO users(phone_number,first_name,last_name,password) VALUES ($1,$2,$3,$4)", rud.PhoneNum, rud.FirstName, rud.LastName, hashedPassword)
	if err != nil {
		c.Error(err)
		return
	}
	log.Printf("user %s registered successfully\n", rud.PhoneNum)
	c.JSON(http.StatusCreated, gin.H{"msg": "created user successfully", "PhoneNum": rud.PhoneNum})
}
func (ct Cntrl) LoginUser(c *gin.Context) {
	var lud LoginUserData
	err := c.BindJSON(&lud)
	if err != nil {
		c.Error(err)
		return
	}
	log.Println(lud.PhoneNum)
	var userPhoneNumber string
	var userPasswordHash string
	var userFirstName string
	err = ct.DbPool.QueryRow(context.Background(), "SELECT phone_number,password,first_name FROM users where phone_number=$1", lud.PhoneNum).Scan(&userPhoneNumber, &userPasswordHash, &userFirstName)
	if err != nil {
		if err == pgx.ErrNoRows {
			log.Println("hash not matched")
			c.Error(errors.New("you have entered incorrect phone number or password"))
			return
		}

		c.Error(err)
		return
	}

	if !CheckPasswordHash(lud.Password, userPasswordHash) {
		c.Error(errors.New("you have entered incorrect phone number or password"))
		return
	}
	sessionId := GetNewUUID()
	ct.SessionStore[sessionId] = UserInfo{
		FirstName:   userFirstName,
		PhoneNumber: userPhoneNumber,
		ExpiredAt:   time.Now().Add(time.Hour),
	}
	log.Printf("user %s logged in successfully\n", userPhoneNumber)
	c.JSON(http.StatusCreated, gin.H{"msg": "user logged in successfully", "sessionToken": sessionId, "firstName": userFirstName})
}

func (ct Cntrl) LogoutUser(c *gin.Context) {
	var lud Logout
	err := c.BindJSON(&lud)
	if err != nil {
		c.Error(err)
		return
	}
	if lud.SToken == "" {
		c.Error(errors.New("session token not present"))
		return
	}
	ud, ok := ct.SessionStore[lud.SToken]
	if !ok {
		c.Error(errors.New("session token is invalid"))
		return
	}

	delete(ct.SessionStore, lud.SToken)
	log.Printf("user %s logged ou\nt", ud.PhoneNumber)
	c.JSON(http.StatusOK, gin.H{"msg": "user logged out successfully"})

}

func (ct Cntrl) ValidateToken(c *gin.Context) {
	var vt Logout
	err := c.BindJSON(&vt)
	if err != nil {
		c.Error(err)
		return
	}
	if vt.SToken == "" {
		c.Error(errors.New("session token not present"))
		return
	}
	ud, ok := ct.SessionStore[vt.SToken]
	if !ok {
		c.Error(errors.New("session token is invalid"))
		return
	}
	if ud.ExpiredAt.Before(time.Now()) {
		c.Error(errors.New("session token has expired"))
		return
	}

	log.Printf("session token validated\n")
	c.JSON(http.StatusCreated, gin.H{"msg": "token validated", "firstName": ud.FirstName, "phoneNumber": ud.PhoneNumber})

}
