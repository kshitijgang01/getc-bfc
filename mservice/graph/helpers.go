package graph

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

type LoginResp struct {
	Msg          string `json:"msg"`
	SessionToken string `json:"sessionToken,omitempty"`
	FirstName    string `json:"firstName,omitempty"`
	PhoneNumber  string `json:"phoneNumber,omitempty"`
}

type CarScan struct {
	ID                string `json:"id"`
	LastServiced      time.Time
	DateOfManufacture time.Time
	LastUsed          time.Time
	Model             string
	Available         bool
}

// authenticating helper to authenticate header of the request/ makes a reques to auth microservice
func Auth(sessionToken string) (string, error) {
	values := map[string]string{"sToken": sessionToken}
	jsonData, err := json.Marshal(values)
	if err != nil {
		return "", err
	}
	resp, err := http.Post("http://localhost:8080/validate", "application/json",
		bytes.NewBuffer(jsonData))
	if err != nil {
		return "", err
	}
	var res LoginResp

	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return "", err
	}
	if resp.StatusCode != http.StatusCreated {

		return "", errors.New(res.Msg)
	}
	return res.PhoneNumber, nil
}

//"BEGIN;DO $do$ BEGIN Select FROM bookings where carId=$1;IF FOUND THEN RAISE EXCEPTION 'Car is already Booked';END IF;update cars set available=FALSE,last_used_date=$3;INSERT INTO booking values ($1,$2,$3);END$do$;COMMIT;"

//"BEGIN;DO $do$ BEGIN DELETE FROM bookings where phoneNumber=$1 and carId=$2;IF NOT FOUND THEN RAISE EXCEPTION 'Booking does not exist';END IF;update cars set available=TRUE,last_used_date=$3;END$do$;COMMIT;"
