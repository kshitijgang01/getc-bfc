package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/ThunderGod77/rMService/mservice/controllers"
	"log"
	"net/http"
	"time"

	"github.com/ThunderGod77/rMService/mservice/graph/generated"
	"github.com/ThunderGod77/rMService/mservice/graph/model"
)

// RegisterUser is the resolver for the registerUser field.
func (r *mutationResolver) RegisterUser(ctx context.Context, input *model.RUser) (*model.User, error) {
	//makes a request to auth microservice to register the new service

	values := map[string]string{"phoneNum": input.PhoneNumber, "firstName": input.FirstName, "lastName": input.LastName, "password": input.Password}
	jsonData, err := json.Marshal(values)
	log.Println(string(jsonData))
	if err != nil {
		return nil, err
	}
	resp, err := http.Post("http://localhost:8080/register", "application/json",
		bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	var res map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&res)
	if resp.StatusCode != http.StatusCreated {
		if errorMessage, ok := res["error"]; ok {
			return nil, errors.New(fmt.Sprintf("%v", errorMessage))
		}
		return nil, errors.New("error while decoding response")
	}
	if _, ok := res["msg"]; ok {
		return &model.User{
			PhoneNumber: input.PhoneNumber,
			FirstName:   input.FirstName,
			LastName:    input.LastName,
		}, nil
	} else {
		return nil, errors.New("error while decoding response")
	}
}

// LoginUser is the resolver for the loginUser field.
func (r *mutationResolver) LoginUser(ctx context.Context, input *model.LUser) (*model.LResponse, error) {
	//makes a request to auth microservice

	values := map[string]string{"phoneNum": input.PhoneNumber, "password": input.Password}
	jsonData, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}
	resp, err := http.Post("http://localhost:8080/login", "application/json",
		bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	var res LoginResp

	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusCreated {

		return nil, errors.New(res.Msg)
	}

	return &model.LResponse{
		SessionToken: res.SessionToken,
		FirstName:    res.FirstName,
	}, nil
}

// LogoutUser is the resolver for the logoutUser field.
func (r *mutationResolver) LogoutUser(ctx context.Context, sessionToken *string) (*model.Message, error) {
	//makes a request to auth microservice

	values := map[string]string{"sessionToken": *sessionToken}
	jsonData, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}
	resp, err := http.Post("https://httpbin.org/post", "application/json",
		bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	var res map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&res)
	if resp.StatusCode != http.StatusOK {
		if errorMessage, ok := res["error"]; ok {
			return nil, errors.New(fmt.Sprintf("%v", errorMessage))
		}
		return nil, errors.New("error while decoding response")
	}
	if msg, ok := res["msg"]; ok {
		return &model.Message{Msg: fmt.Sprintf("%v", msg)}, nil
	} else {
		return nil, errors.New("error while decoding response")
	}
}

// BookCar is the resolver for the bookCar field.
func (r *mutationResolver) BookCar(ctx context.Context, input model.Bkcar) (*model.Message, error) {
	//executes the transaction to book_car procedure which updates the booking table and cars table

	sToken := ctx.Value("auth")
	if sToken == nil {
		return nil, errors.New("cannot authenticate")
	}
	stoken := sToken.(controllers.Stoken)
	var phoneNum string
	var err error
	if phoneNum, err = Auth(string(stoken)); err != nil {
		return nil, err
	}
	tx, err := r.DbPool.Begin(ctx)
	if err != nil {
		return nil, err
	}
	_, err = tx.Exec(context.Background(), "call book_car($1,$2,$3);", input.CarID, phoneNum, time.Now().Format(time.RFC3339))
	log.Println(err)
	if err != nil {
		return nil, err
	}
	err = tx.Commit(context.Background())

	if err != nil {
		return nil, err
	}

	return &model.Message{Msg: "booked car successfully"}, nil
}

// ReturnCar is the resolver for the returnCar field.
func (r *mutationResolver) ReturnCar(ctx context.Context, input model.Bkcar) (*model.Message, error) {
	//executes the transaction to return_car procedure which updates the booking table and cars table

	sToken := ctx.Value("auth")
	if sToken == nil {
		return nil, errors.New("cannot authenticate")
	}
	stoken := sToken.(controllers.Stoken)
	var phoneNum string
	var err error
	if phoneNum, err = Auth(string(stoken)); err != nil {
		return nil, err
	}
	tx, err := r.DbPool.Begin(ctx)
	if err != nil {
		return nil, err
	}
	_, err = tx.Exec(context.Background(), "call return_car($1,$2,$3);", input.CarID, phoneNum, time.Now().Format(time.RFC3339))
	if err != nil {
		return nil, err
	}
	err = tx.Commit(context.Background())

	if err != nil {
		return nil, err
	}
	return &model.Message{Msg: "returned car successfully"}, nil
}

// Cars is the resolver for the cars field.
func (r *queryResolver) Cars(ctx context.Context, available *bool) ([]*model.Car, error) {
	//queries cars from the database

	var query string

	if *available {
		query = "SELECT * from cars WHERE available=TRUE"
	} else {
		query = "SELECT * from cars"
	}
	rows, err := r.DbPool.Query(context.Background(), query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var c CarScan
	var cars []*model.Car
	for rows.Next() {
		err = rows.Scan(&c.ID, &c.LastServiced, &c.DateOfManufacture, &c.LastUsed, &c.Model, &c.Available)
		if err != nil {
			return nil, err
		}
		cars = append(cars, &model.Car{
			ID:                c.ID,
			LastServiced:      c.LastServiced.String(),
			LastUsed:          c.LastUsed.String(),
			DateOfManufacture: c.DateOfManufacture.String(),
			Model:             c.Model,
			Available:         c.Available,
		})
	}

	return cars, err
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
