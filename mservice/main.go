package main

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/ThunderGod77/rMService/mservice/controllers"
	"github.com/ThunderGod77/rMService/mservice/graph"
	"github.com/ThunderGod77/rMService/mservice/graph/generated"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/sync/errgroup"
)

var (
	g errgroup.Group
)

// Error handler for authentication microservice
func ErrorHandle() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		err := c.Errors.Last()
		if err == nil {
			return
		}

		c.JSON(400, gin.H{"msg": err.Error()})
		return
	}
}

// authentication microservice
func router01() http.Handler {
	dbpool, err := pgxpool.New(context.Background(), "postgresql://postgres:password@db/flcbook")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create connection pool: %v\n", err)
		os.Exit(1)
	}
	m := make(map[string]controllers.UserInfo)
	c := controllers.Cntrl{DbPool: dbpool, SessionStore: m}
	e := gin.Default()

	e.POST("/register", c.RegisterUser)
	e.POST("/login", ErrorHandle(), c.LoginUser)
	e.POST("/logout", ErrorHandle(), c.LogoutUser)
	e.POST("/validate", ErrorHandle(), c.ValidateToken)
	e.GET("/", func(c *gin.Context) {
		c.JSON(
			http.StatusOK,
			gin.H{
				"code":  http.StatusOK,
				"error": "Welcome server 01",
			},
		)
	})

	return e
}

// handler for booking microservice
func graphqlHandler(pool *pgxpool.Pool) gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	h := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{DbPool: pool}}))
	return func(c *gin.Context) {
		auth := c.GetHeader("Authorization")
		st := controllers.Stoken(auth)
		ctx := context.WithValue(c.Request.Context(), "auth", st)
		c.Request = c.Request.WithContext(ctx)
		h.ServeHTTP(c.Writer, c.Request)
	}
}

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")
	return func(c *gin.Context) {

		h.ServeHTTP(c.Writer, c.Request)
	}
}

// booking microservice
func router02() http.Handler {
	dbpool, err := pgxpool.New(context.Background(), "postgresql://postgres:password@db/flcbook")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create connection pool: %v\n", err)
		os.Exit(1)
	}
	e := gin.Default()
	e.POST("/query", graphqlHandler(dbpool))
	e.GET("/", playgroundHandler())

	return e
}

func main() {
	server01 := &http.Server{
		Addr:         ":8080",
		Handler:      router01(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	server02 := &http.Server{
		Addr:         ":8081",
		Handler:      router02(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	//starting auth service
	g.Go(func() error {
		err := server01.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
		return err
	})
	//starting booking service
	g.Go(func() error {
		err := server02.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
		return err
	})

	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}
}
